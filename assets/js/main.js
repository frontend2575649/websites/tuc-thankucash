(function ($) {
    "use strict";



    // meanmenu
    $('.bar').click(function () {
        $('.offcanvas__wrapper, .overlay').addClass('active');
    });
    $('.overlay').click(function () {
        $('.offcanvas__wrapper, .overlay').removeClass('active');
    });




    // marchent slider js
    $('.marchent__slide').slick({
        speed: 3000,
        autoplay: true,
        autoplaySpeed: 0,
        centerMode: true,
        cssEase: 'linear',
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true,
        infinite: true,
        initialSlide: 1,
        arrows: false,
        buttons: false,
        infinite: true,
        pauseOnHover: false,
        pauseOnFocus: false,
    });





    // Intern carosel
    $('.intern__slide').owlCarousel({
        loop: true,
        margin: 55,
        items: 1,
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        nav: false,
        dots: false,
        autoplay: true,
        autoplayTimeout: 4000,
        responsive: {
            0: {
                items: 3,
                margin: 10
            },
            767: {
                items: 4
            },
            992: {
                items: 6,
                margin: 30,
            },
            1500: {
                items: 6,
            }
        }
    });

    // testimonial carosel
    $('.testimonial__active').owlCarousel({
        loop: true,
        margin: 0,
        items: 1,
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        nav: false,
        dots: true,
        autoplay: true,
        autoplayTimeout: 4000,
    });



    $('.hero-slider-active').owlCarousel({
        loop: true,
        margin: 0,
        items: 1,
        navText: ['<i class="fa fa-arrow-left"></i>', '<i class="fa fa-arrow-right"></i>'],
        nav: false,
        dots: true,
        autoplay: true,
        autoplayTimeout: 4000,
    });


    // $('.deals-product-slider-active').owlCarousel({
    //     loop: true,
    //     margin: 0,
    //     items: 1,
    //     navText: ['<i class="fa fa-arrow-left"></i>', '<i class="fa fa-arrow-right"></i>'],
    //     nav: true,
    //     dots: false,
    //     autoplay: true,
    //     autoplayTimeout: 4000,
    // });









    // scrollToTop
    $.scrollUp({
        scrollName: 'scrollUp', // Element ID
        topDistance: '300', // Distance from top before showing element (px)
        topSpeed: 300, // Speed back to top (ms)
        animation: 'fade', // Fade, slide, none
        animationInSpeed: 200, // Animation in speed (ms)
        animationOutSpeed: 200, // Animation out speed (ms)
        scrollText: '<i class="far fa-long-arrow-up"></i>', // Text for element
        activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
    });

    var wow = new WOW({
        boxClass: 'wow',
        animateClass: 'animated',
        mobile: false,
        offset: 50
    }
    );

    wow.init();


})(jQuery);



$(document).ready(function () {

    // type
    try{
            var typed = new Typed('.typed', {
        strings: ["Airtime.", "Grocery.", "Fuel.", "Drink."],
        typeSpeed: 100,
        backSpeed: 100,
        loop: true,
        smartBackspace: true, // this is a defa
    });
    }catch(err){
        console.log(err)
    }
    try {
        const ofsetOfTwo = document.getElementById('#two').offset().top;
        window.onscroll = function (e) {
            if (window.scrollY === divTowPosition) {
                //Do stuff...
            }
        }
    } catch (error) {
        console.log(error);
    }



    //    $('.how-work-box').mouseover(function(){
    //        $('.how-work-box').removeClass('hover');
    //        $(this).addClass('hover');
    //    })
    //    $('.how-work-box').click(function(){
    //     $('.how-work-box').removeClass('hover');
    //     $(this).addClass('hover');
    //    })


    //    var contG = $('.option-x-grid')
    //    $('.option-x-grid').attr('child-active','1');
    //    setInterval(function() {
    //     $(contG).each(function(){

    // console.log('s');
    //         var childL = $(this).find('.option-x-grid-item').length;
    //         var childN = $(this).attr('child-active');
    //         if (childN  < childL) {
    //         $('.option-x-grid').attr('child-active', (childN - -1));
    //         } else {
    //         $('.option-x-grid').attr('child-active', '1');
    //         }
    //         // console.log(childL)

    //     });
    //     }, 1000);




    // $('.option-x-grid').attr('child-active','1');



    // setInterval(function() {
    //     var containerG = '.option-x-grid1';
    //     var containerE = '.option-x-grid-item'
    //     var contG = $(containerG);
    //     var contE = contG.find(containerE);
    //     var childL = contE.length;
    //     var childN = contG.attr('child-active');
    //     contE.removeClass('active');
    //     if (childN  < childL) {
    //         $('.option-x-grid').attr('child-active', (childN - -1));
    //         var selector = (containerG + " " + containerE + ":nth-child(" + (childN - -1) + ")");
    //         $(selector).addClass('active');
    //         console.log(selector);
    //     } else {
    //     $('.option-x-grid').attr('child-active', '1');
    //     $(containerG + " " + containerE + ":nth-child(1)").addClass('active');
    //     }
    //  }, 1000);




    //     //  class-animation
    //    function classAnimation($containerG,$containerE){
    //     $($containerG).attr('child-active','1');
    //     $($containerG + " " + $containerE + ":nth-child(1)").addClass('active');
    //     setInterval(function() {
    //         let containerG = $containerG;
    //         let containerE = $containerE;
    //         let contG = $(containerG);
    //         let contE = contG.find(containerE);
    //         let childL = contE.length;
    //         let childN = contG.attr('child-active');
    //         contE.removeClass('active');
    //         if (childN  < childL) {
    //             $('.option-x-grid').attr('child-active', (childN - -1));
    //             let selector = (containerG + " " + containerE + ":nth-child(" + (childN - -1) + ")");
    //             $(selector).addClass('active');
    //             console.log(selector);
    //         } else {
    //         $('.option-x-grid').attr('child-active', '1');
    //         $(containerG + " " + containerE + ":nth-child(1)").addClass('active');
    //         }
    //      }, 1000);
    //    }
    //    classAnimation(".option-x-grid1",".option-x-grid-item");
    //    // class-animation2
    //    function classAnimation2($containerG,$containerE){
    //     $($containerG).attr('child-active','1');
    //     $($containerG + " " + $containerE + ":nth-child(1)").addClass('active');
    //     setInterval(function() {
    //         let containerG = $containerG;
    //         let containerE = $containerE;
    //         let contG = $(containerG);
    //         let contE = contG.find(containerE);
    //         let childL = contE.length;
    //         let childN = contG.attr('child-active');
    //         contE.removeClass('active');
    //         if (childN  < childL) {
    //             $('.option-x-grid').attr('child-active', (childN - -1));
    //             let selector = (containerG + " " + containerE + ":nth-child(" + (childN - -1) + ")");
    //             $(selector).addClass('active');
    //             console.log(selector);
    //         } else {
    //         $('.option-x-grid').attr('child-active', '1');
    //         $(containerG + " " + containerE + ":nth-child(1)").addClass('active');
    //         }
    //      }, 1000);
    //    }
    // //    classAnimation2(".option-x-grid3",".option-x-grid-item");



});




